#ifndef LINSM_H_ //[SWS_LinSM_00005]
#define LINSM_H_

/// 8.1 Imported types

//[SWS_LinSM_00219]
#include "Rte_ComM_Type.h"
#include "ComStackTypes.h"
#include "LinIfMock.h"				//[SWS_LinSM_00001]
#include "LinTrcv.h"
#include "StandardTypes.h"

//
//[SWS_LinSM_00085] N/A: brak w sepcyfikacji informacji o interakcji
//[SWS_LinSM_00002] OK: LinSM nie uzywa Lin driver

///

/// IDs definiton
#define LINSM_MODULE_ID							255
#define LINSM_INIT_SERVICE_ID                   0x01
#define LINSM_GET_CURRENT_COM_MODE_SERVICE_ID   0x11
#define LINSM_REQUEST_COM_MODE_SERVICE_ID       0x12
#define LINSM_SCHEDULE_REQUEST_SERVICE_ID       0x10
#define LINSM_SCHEDULE_REQUEST_CONF_SERVICE_ID  0x20
#define LINSM_WAKEUP_CONF_SERVICE_ID            0x21
#define LINSM_GOTO_SLEEP_CONF_SERVICE_ID        0x22
#define LINSM_MAIN_FUNCTION_SERVICE_ID          0x30
///

/// 7.3 Error Codes
//[SWS_LinSM_00053]
#define LINSM_E_UNINIT                  0x00
#define LINSM_E_NONEXISTENT_NETWORK		0x20
#define LINSM_E_PARAMETER				0x30
#define LINSM_E_PARAMETER_POINTER		0x40
#define LINSM_E_INIT_FAILED				0x50
//
//[SWS_LinSM_00224]
#define LINSM_E_CONFIRMATION_TIMEOUT	0x00
//
///

//[SWS_LinSM_00225] OK: 7.3.3 Transient Faults
//[SWS_LinSM_00226] OK: 7.3.4 Production Errors
//[SWS_LinSM_00227] OK: 7.3.5 Extended Production Errors


/// 8.2 Type definitions

//[SWS_LinSM_00220]
typedef enum LinSM_ModeType {
	LINSM_FULL_COM = 0x01,
	LINSM_NO_COM = 0x02
} LinSM_ModeType;
//

//[SWS_LinSM_00221]
typedef struct LinSM_ConfigType {
	uint8_t empty;
}LinSM_ConfigType;

///

/// 8.3 LinSM API

//[SWS_LinSM_00155]
void LinSM_Init(const LinSM_ConfigType* ConfigPtr);
//

//[SWS_LinSM_00113]
Std_ReturnType LinSM_ScheduleRequest(NetworkHandleType network, LinIf_SchHandleType schedule);	//[SWS_LinSM_00241] N/A: konfiguracja tylko dla mastera
//

//[SWS_LinSM_00117]
void LinSM_GetVersionInfo(Std_VersionInfoType* versioninfo);
//

//[SWS_LinSM_00122]
Std_ReturnType LinSM_GetCurrentComMode(NetworkHandleType network, ComM_ModeType* mode);
//

//[SWS_LinSM_00126]
Std_ReturnType LinSM_RequestComMode(NetworkHandleType network, ComM_ModeType mode);
//

///

/// 8.4 Scheduled Functions
//[SWS_LinSM_00156]
void LinSM_MainFunction(void);
//
///

#endif /* LINSM_H_ */