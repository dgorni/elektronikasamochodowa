#ifndef STANDARD_TYPES_H
#define STANDARD_TYPES_H

#include <stdint.h>
#include <stdbool.h>

typedef struct {
	uint16_t vendorID;
	uint16_t moduleID;
	uint8_t  instanceID;

	uint8_t sw_major_version;
	uint8_t sw_minor_version;
	uint8_t sw_patch_version;
} Std_VersionInfoType;


typedef uint8_t Std_ReturnType;

#define E_OK 					(Std_ReturnType)0
#define E_NOT_OK 				(Std_ReturnType)1

#endif /*STANDARD_TYPES_H*/