#pragma once

#include "BswM_LinSM.h"

#ifndef UNIT_TEST
#ifndef GMOCKIN
#define GMOCKIN
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "src/gmock-cardinalities.cc"
#include "src/gmock-internal-utils.cc"
#include "src/gmock-matchers.cc"
#include "src/gmock-spec-builders.cc"
#include "src/gmock.cc"

#include "src/gtest.cc"
#include "src/gtest-death-test.cc"
#include "src/gtest-filepath.cc"
#include "src/gtest-port.cc"
#include "src/gtest-printers.cc"
#include "src/gtest-test-part.cc"
#include "src/gtest-typed-test.cc"

#endif // !GMOCKIN
#endif // !UNIT_TEST




class BswM_LinSM {
public:
    ~BswM_LinSM() {}
    virtual void BswM_LinSM_CurrentSchedule(NetworkHandleType Network, LinIf_SchHandleType CurrentSchedule) = 0;
    virtual void BswM_LinSM_CurrentState(NetworkHandleType Network, LinSM_ModeType CurrentState) = 0;
};

class BswM_LinSMMock : public BswM_LinSM {
public:
    MOCK_METHOD2(BswM_LinSM_CurrentSchedule, void(NetworkHandleType Network, LinIf_SchHandleType CurrentSchedule));
    MOCK_METHOD2(BswM_LinSM_CurrentState, void(NetworkHandleType Network, LinSM_ModeType CurrentState));

};
