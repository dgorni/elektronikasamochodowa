#pragma once

#include "Det.h"

#ifndef UNIT_TEST
#ifndef GMOCKIN
#define GMOCKIN
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "src/gmock-cardinalities.cc"
#include "src/gmock-internal-utils.cc"
#include "src/gmock-matchers.cc"
#include "src/gmock-spec-builders.cc"
#include "src/gmock.cc"

#include "src/gtest.cc"
#include "src/gtest-death-test.cc"
#include "src/gtest-filepath.cc"
#include "src/gtest-port.cc"
#include "src/gtest-printers.cc"
#include "src/gtest-test-part.cc"
#include "src/gtest-typed-test.cc"

#endif // !GMOCKIN

#endif // !UNIT_TEST




class Det {
public:
    ~Det() {}
    virtual Std_ReturnType Det_ReportError(uint16_t ModuleId, uint8_t InstanceId, uint8_t ApiId, uint8_t ErrorId)  = 0;
    virtual Std_ReturnType Det_ReportRuntimeError(uint16_t ModuleId, uint8_t InstanceId, uint8_t ApiId, uint8_t ErrorId)  = 0;
};

class DetMock : public Det{
public:
    MOCK_METHOD4(Det_ReportError, Std_ReturnType(uint16_t ModuleId, uint8_t InstanceId, uint8_t ApiId, uint8_t ErrorId));
    MOCK_METHOD4(Det_ReportRuntimeError, Std_ReturnType(uint16_t ModuleId, uint8_t InstanceId, uint8_t ApiId, uint8_t ErrorId));

};


#include "LinIf.h"	

class LinIf {
public:
    ~LinIf() {}
    virtual Std_ReturnType LinIf_ScheduleRequest(NetworkHandleType Channel, LinIf_SchHandleType Schedule) = 0;
    virtual Std_ReturnType LinIf_GotoSleep(NetworkHandleType Channel) = 0;
    virtual Std_ReturnType LinIf_WakeUp(NetworkHandleType Channel) = 0;
};

class LinIfMock : public LinIf {
public:
    MOCK_METHOD2(LinIf_ScheduleRequest, Std_ReturnType(NetworkHandleType Channel, LinIf_SchHandleType Schedule));
    MOCK_METHOD1(LinIf_GotoSleep, Std_ReturnType(NetworkHandleType Channel));
    MOCK_METHOD1(LinIf_WakeUp, Std_ReturnType(NetworkHandleType Channel));

};