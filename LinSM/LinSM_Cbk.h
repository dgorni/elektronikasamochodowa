#ifndef LINSM_CBK_H_
#define LINSM_CBK_H_

#include "LinSM.h"

/// 8.5 LinSM callbacks

//[SWS_LinSM_00129]
void LinSM_ScheduleRequestConfirmation(NetworkHandleType network, LinIf_SchHandleType schedule);
//

//[SWS_LinSM_91000] #NOTIN - slave node
void LinSM_GotoSleepIndication(NetworkHandleType Channel);
//

//[SWS_LinSM_00135]
void LinSM_GotoSleepConfirmation(NetworkHandleType network, bool success);
//

//[SWS_LinSM_00132]
void LinSM_WakeupConfirmation(NetworkHandleType network, bool success);
//

///

#endif /*LINSM_CBK_H_*/