#include "LinSM.h"
#include "SchM_LinSm.h"
#include "LinSM_Cfg.h"
#include "LinSM_Cbk.h"

	

#include "ComMMock.h"			//[SWS_LinSM_00105] [SWS_LinSM_00013]
#include "DetMock.h"			//[SWS_LinSM_00086]
#include "BswM_LinSMMock.h"		//[SWS_LinSM_00196] [SWS_LinSM_00201]
//[SWS_LinSM_00305 ] N/A: modul nie uzywany
//[SWS_LinSM_00208] N/A: opcjonalne, brak header consistency check

//// TYPES DEFINITION

#define NULL_SCHEDULE 0
#define NULL_PTR 0

enum LinSM_state_t {
	LINSM_UNINIT,	//[SWS_LinSM_00022]
	LINSM_INIT		//[SWS_LinSM_00024]
} ;


struct NetworkStatus_t {
	LinSM_ModeType LinSM_substate;				////[SWS_LinSM_00173]
	LinIf_SchHandleType LinSM_SchTablCurr;
	LinIf_SchHandleType LinSM_SchTablNew;
	uint16_t ScheduleRequestTimer;
	uint16_t GoToSleepTimer;
	uint16_t WakeUpTimer;
};

//// GLOBAL VARIABLE DEFINITION
static enum LinSM_state_t LinSM_state = LINSM_UNINIT; //[SWS_LinSM_00020] [SWS_LinSM_00161]

static struct NetworkStatus_t NetworkStatusTable[LINIF_NUMBER_OF_LIN_NETWORK]; //[SWS_LinSM_00019] [SWS_LinSM_00021] [SWS_LinSM_00164]

DetMock MyDet;
BswM_LinSMMock MyBswM_LinSM;
ComMMock MyComM;
LinIfMock MyLinIf;

//// API FUNCTION DEFINITION
void LinSM_Init(const LinSM_ConfigType* ConfigPtr)
{
	uint8_t NetworkCounter;
	for (NetworkCounter = 0; LINIF_NUMBER_OF_LIN_NETWORK > NetworkCounter; NetworkCounter++)	//[SWS_LinSM_00043]
	{
		NetworkStatusTable[NetworkCounter].LinSM_substate = LINSM_NO_COM;		//[SWS_LinSM_00152] [SWS_LinSM_00160] [SWS_LinSM_00026]
		NetworkStatusTable[NetworkCounter].LinSM_SchTablCurr = NULL_SCHEDULE;	//[SWS_LinSM_00216]
		NetworkStatusTable[NetworkCounter].LinSM_SchTablNew = NULL_SCHEDULE;
		NetworkStatusTable[NetworkCounter].ScheduleRequestTimer = 0;			//[SWS_LinSM_00175]
		NetworkStatusTable[NetworkCounter].GoToSleepTimer = 0;
		NetworkStatusTable[NetworkCounter].WakeUpTimer = 0;
	}
	LinSM_state = LINSM_INIT;		//[SWS_LinSM_00025]

}	//[SWS_LinSM_00166] OK: no call; [SWS_LinSM_00204] [SWS_LinSM_00151]



Std_ReturnType LinSM_ScheduleRequest(NetworkHandleType network, LinIf_SchHandleType schedule)
{
	Std_ReturnType result;

	if ((CFG_LinSMMainProcessingPeriod <= NetworkStatusTable[network].ScheduleRequestTimer) || (LINSM_FULL_COM != NetworkStatusTable[network].LinSM_substate))	//[SWS_LinSM_00163] [SWS_LinSM_10211]
	{
		result = E_NOT_OK;
	}
	else {
		NetworkStatusTable[network].ScheduleRequestTimer = CFG_LinSMConfirmationTimeout[network];	//[SWS_LinSM_00100]
		NetworkStatusTable[network].LinSM_SchTablNew = schedule;
		result = MyLinIf.LinIf_ScheduleRequest(network, schedule);			//[SWS_LinSM_00079] [SWS_LinSM_00168]
		if (result != E_OK) {
			MyBswM_LinSM.BswM_LinSM_CurrentSchedule(network, NetworkStatusTable[network].LinSM_SchTablCurr);
			NetworkStatusTable[network].LinSM_SchTablNew = NetworkStatusTable[network].LinSM_SchTablCurr; //[SWS_LinSM_00213]
			NetworkStatusTable[network].ScheduleRequestTimer = 0;
		}
	}
	return result;
}
//[SWS_LinSM_00114] [SWS_LinSM_00115] [SWS_LinSM_00116] N/A: konfiguracja dla LinSMDevErrorDetect = false

void LinSM_GetVersionInfo(Std_VersionInfoType* versioninfo)
{
}
//[SWS_LinSM_00119] N/A: konfiguracja dla LinSMDevErrorDetect = false

Std_ReturnType LinSM_GetCurrentComMode(NetworkHandleType network, ComM_ModeType* mode)
{
	if (network < LINIF_NUMBER_OF_LIN_NETWORK)
	{
		if (LinSM_state == LINSM_UNINIT && mode != NULL_PTR)	
		{
			*mode = COMM_NO_COMMUNICATION;	//[SWS_LinSM_00182]
		}
	}

	switch (NetworkStatusTable[network].LinSM_substate)
	{
	case LINSM_FULL_COM:					//[SWS_LinSM_00032]
		*mode = COMM_FULL_COMMUNICATION;	//[SWS_LinSM_00181]
		break;
	case LINSM_NO_COM:
		*mode = COMM_NO_COMMUNICATION;		//[SWS_LinSM_00028] ////[SWS_LinSM_00180]
	default:
		*mode = COMM_NO_COMMUNICATION;
		break;
	}
	return E_OK;
}
//[SWS_LinSM_00123] [SWS_LinSM_00124] [SWS_LinSM_00125] N/A: konfiguracja dla LinSMDevErrorDetect = false

Std_ReturnType LinSM_RequestComMode(NetworkHandleType network, ComM_ModeType mode)
{
	Std_ReturnType result = E_NOT_OK;

	switch (mode)
	{
	case COMM_NO_COMMUNICATION:
		NetworkStatusTable[network].GoToSleepTimer = CFG_LinSMConfirmationTimeout[network];	//[SWS_LinSM_00100]
		if (E_OK == MyLinIf.LinIf_GotoSleep(network)) {		//[SWS_LinSM_10208] [SWS_LinSM_10209]
			NetworkStatusTable[network].LinSM_substate = LINSM_NO_COM; //[SWS_LinSM_00302] [SWS_LinSM_00223]
			result = E_OK;
		}
		else {
			MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_FULL_COM);				//[SWS_LinSM_00192]
			MyComM.ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION);	//[SWS_LinSM_00033]
			NetworkStatusTable[network].GoToSleepTimer = 0;					
			result = E_NOT_OK;												//[SWS_LinSM_00177]
		}
		break;
	case COMM_SILENT_COMMUNICATION:
		result = E_NOT_OK;		//[SWS_LinSM_00183]
		break;
	case COMM_FULL_COMMUNICATION:
		NetworkStatusTable[network].WakeUpTimer = CFG_LinSMConfirmationTimeout[network];	//[SWS_LinSM_00301] [SWS_LinSM_00100]
		if (E_OK == MyLinIf.LinIf_WakeUp(network)) {		//[SWS_LinSM_00047] [SWS_LinSM_00178]
			result = E_OK;
		}
		else
		{
			MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_NO_COM);				//[SWS_LinSM_00193] EXAMPLE
			MyComM.ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION);	//[SWS_LinSM_00027] EXAMPLE
			NetworkStatusTable[network].WakeUpTimer = 0;
			result = E_NOT_OK;	//[SWS_LinSM_00176]
		}
		break;
	default:
		break;
	}
	return result;
}	//[SWS_LinSM_00035] N/A: optionl brak sprawdzenia; [SWS_LinSM_00036] N/A: optional
//[SWS_LinSM_00127] [SWS_LinSM_00191] [SWS_LinSM_00128] N/A: konfiguracja dla LinSMDevErrorDetect = false


void LinSM_ScheduleRequestConfirmation(NetworkHandleType network, LinIf_SchHandleType schedule)
{
	if (CFG_LinSMMainProcessingPeriod <= NetworkStatusTable[network].ScheduleRequestTimer)	//[SWS_LinSM_00172]
	{
		NetworkStatusTable[network].LinSM_SchTablCurr = NetworkStatusTable[network].LinSM_SchTablNew;
		MyBswM_LinSM.BswM_LinSM_CurrentSchedule(network, NetworkStatusTable[network].LinSM_SchTablCurr);		//[SWS_LinSM_00206] [SWS_LinSM_00207]
	}
	NetworkStatusTable[network].ScheduleRequestTimer = 0; //[SWS_LinSM_00154]
}	
//[SWS_LinSM_00130] [SWS_LinSM_00131] N/A: konfiguracja dla LinSMDevErrorDetect = false

void LinSM_GotoSleepIndication(NetworkHandleType Channel)
{
//[SWS_LinSM_00239] [SWS_LinSM_00240] [SWS_LinSM_00243] N/A: slave
}

void LinSM_GotoSleepConfirmation(NetworkHandleType network, bool success)
{
	if (CFG_LinSMMainProcessingPeriod <= NetworkStatusTable[network].GoToSleepTimer)	//[SWS_LinSM_00172]
	{
		if (true == success)
		{
			NetworkStatusTable[network].LinSM_substate = LINSM_NO_COM;
			MyComM.ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION);
			MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_NO_COM);
		}
		else {
			MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_FULL_COM);
			MyComM.ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION);
		}
	}
	NetworkStatusTable[network].GoToSleepTimer = 0;	//[SWS_LinSM_00154]
}	//[SWS_LinSM_00046] N/A: optional, brak sprawdzenia
//[SWS_LinSM_00136] [SWS_LinSM_00137] N/A: konfiguracja dla LinSMDevErrorDetect = false

void LinSM_WakeupConfirmation(NetworkHandleType network, bool success)
{
	if (CFG_LinSMMainProcessingPeriod <= NetworkStatusTable[network].WakeUpTimer) //[SWS_LinSM_00172]
	{
		if (true == success)
		{
			MyComM.ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION);
			NetworkStatusTable[network].LinSM_substate = LINSM_FULL_COM; //[SWS_LinSM_00049]
			MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_FULL_COM);
		}
		else {	//[SWS_LinSM_00202]
			MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_NO_COM);
			MyComM.ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION);
		}
	}
	NetworkStatusTable[network].WakeUpTimer = 0;	//[SWS_LinSM_00154]
}
//[SWS_LinSM_00133] [SWS_LinSM_00134] N/A: konfiguracja dla LinSMDevErrorDetect = false

void LinSM_DeInit()
{
	LinSM_state = LINSM_UNINIT;
}

void DecrementTimer(uint16_t* timer) {
	*timer = *timer - CFG_LinSMMainProcessingPeriod;	//[SWS_LinSM_00159]
	if (*timer < CFG_LinSMMainProcessingPeriod)			//[SWS_LinSM_00101] 
	{	
		MyDet.Det_ReportRuntimeError(LINSM_MODULE_ID, 0, LINSM_MAIN_FUNCTION_SERVICE_ID, LINSM_E_CONFIRMATION_TIMEOUT);	//[SWS_LinSM_00102]
	}
}

void LinSM_MainFunction(void)
{
	NetworkHandleType network;

	for (network = 0; LINIF_NUMBER_OF_LIN_NETWORK > network; network++) 
	{
		if (CFG_LinSMMainProcessingPeriod <= NetworkStatusTable[network].ScheduleRequestTimer) 
		{
			DecrementTimer(&NetworkStatusTable[network].ScheduleRequestTimer);
			if (CFG_LinSMMainProcessingPeriod > NetworkStatusTable[network].ScheduleRequestTimer) 
			{
				MyBswM_LinSM.BswM_LinSM_CurrentSchedule(network, NetworkStatusTable[network].LinSM_SchTablCurr);	//[SWS_LinSM_00214]
			}
		}
		
		if (CFG_LinSMMainProcessingPeriod <= NetworkStatusTable[network].GoToSleepTimer)
		{
			DecrementTimer(&NetworkStatusTable[network].GoToSleepTimer);
			if (CFG_LinSMMainProcessingPeriod > NetworkStatusTable[network].GoToSleepTimer)
			{
				MyComM.ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION);	//[SWS_LinSM_00170]
				MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_FULL_COM);				//[SWS_LinSM_00215]
			}
		}

		if (CFG_LinSMMainProcessingPeriod <= NetworkStatusTable[network].WakeUpTimer)
		{
			DecrementTimer(&NetworkStatusTable[network].WakeUpTimer);
			if (CFG_LinSMMainProcessingPeriod > NetworkStatusTable[network].WakeUpTimer)
			{
				MyComM.ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION);	//[SWS_LinSM_00170]
				MyBswM_LinSM.BswM_LinSM_CurrentState(network, LINSM_NO_COM);				//[SWS_LinSM_00215]
			}
		}
	}
} //[SWS_LinSM_00162] [SWS_LinSM_00157]

//[SWS_LinSM_00103] OK: timery zostana wylaczone
//[SWS_LinSM_00229] OK: wszytskie interfejsy zapewnione
//[SWS_LinSM_00073] OK: brak consistency check of the configuration in run-time
//[SWS_LinSM_00211] OK: brak aplikacji specyfikacji
//[SWS_LinSM_00203] [SWS_LinSM_00205]  N/A: optional, brak PassivMode dla Transceivera
//[SWS_LinSM_00230] [SWS_LinSM_00231] [SWS_LinSM_00232] [SWS_LinSM_00233] [SWS_LinSM_00234] N/A: slave
//[SWS_LinSM_00304] [SWS_LinSM_00307] N/A: konfiguracja tylko dla LinSMModeRequestRepetitionMax = 1
//[SWS_LinSM_00235] [SWS_LinSM_00236] [SWS_LinSM_00237] N/A: slave
//[SWS_LinSM_00138] N/A: optional, brak tych interfesjow



#ifndef UNIT_TEST

int main()
{

}

#endif // !UNIT_TEST
