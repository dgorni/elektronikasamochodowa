#pragma once

#include "ComM.h"

#ifndef UNIT_TEST
#ifndef GMOCKIN
#define GMOCKIN
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "src/gmock-cardinalities.cc"
#include "src/gmock-internal-utils.cc"
#include "src/gmock-matchers.cc"
#include "src/gmock-spec-builders.cc"
#include "src/gmock.cc"

#include "src/gtest.cc"
#include "src/gtest-death-test.cc"
#include "src/gtest-filepath.cc"
#include "src/gtest-port.cc"
#include "src/gtest-printers.cc"
#include "src/gtest-test-part.cc"
#include "src/gtest-typed-test.cc"

#endif // !GMOCKIN
#endif // !UNIT_TEST




class ComM {
public:
    ~ComM() {}
    virtual void ComM_BusSM_ModeIndication(NetworkHandleType Channel, ComM_ModeType ComMode) = 0;
};

class ComMMock : public ComM {
public:
    MOCK_METHOD2(ComM_BusSM_ModeIndication, void(NetworkHandleType Channel, ComM_ModeType ComMode));
};
