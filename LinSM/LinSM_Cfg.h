#ifndef LINSM_CFG_H_
#define LINSM_CFG_H_

#include <stdint.h>
#include <stdbool.h>
#include "LinIf.h"

typedef enum LinSMNodeType_t {
	MASTER,
	SLAVE
} LinSMNodeType_t;

//  Module LinSM

	//  Container LinSMConfigSet
		 const uint8_t CFG_LinSMModeRequestRepetitionMax = 1;	
		//  Container LinSMChannel
			 const uint32_t CFG_LinSMConfirmationTimeout[LINIF_NUMBER_OF_LIN_NETWORK] = { 2, 2, 2 };
			 const LinSMNodeType_t CFG_LinSMNodeType[LINIF_NUMBER_OF_LIN_NETWORK] = { MASTER, MASTER, MASTER };
			 const uint32_t CFG_LinSMSilenceAfterWakeupTimeout[LINIF_NUMBER_OF_LIN_NETWORK] = { 1, 1, 1 };
			 const bool CFG_LinSMTransceiverPassiveMode[LINIF_NUMBER_OF_LIN_NETWORK] = { false, false, false };
			//  Item LinSMComMNetworkHandleRef
				//  Container LinSMSchedule
			 const uint8_t CFG_LinSMScheduleIndex[LINIF_NUMBER_OF_LIN_NETWORK] = { 0, 1, 2 };
			 const bool CFG_LinSMScheduleIndexRef[LINIF_NUMBER_OF_LIN_NETWORK] = { true, true, true };
				// :Container LinSMSchedule
			// :Item LinSMComMNetworkHandleRef
		// :Container LinSMChannel

	// :Container LinSMConfigSet

	//  Container LinSMGeneral
		 const bool CFG_LinSMDevErrorDetect = false;
		 const uint16_t CFG_LinSMMainProcessingPeriod = 1;
		 const bool CFG_LinSMVersionInfoApi = false;

	// :Container LinSMGeneral

// :Module LinSM

#endif /*LINSM_CFG_H_*/