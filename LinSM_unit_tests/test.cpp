#include "pch.h"

#define UNIT_TEST

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "src/gmock-cardinalities.cc"
#include "src/gmock-internal-utils.cc"
#include "src/gmock-matchers.cc"
#include "src/gmock-spec-builders.cc"
#include "src/gmock.cc"

#include "src/gtest.cc"
#include "src/gtest-death-test.cc"
#include "src/gtest-filepath.cc"
#include "src/gtest-port.cc"
#include "src/gtest-printers.cc"
#include "src/gtest-test-part.cc"
#include "src/gtest-typed-test.cc"

#include "../LinSM/LinSM.cpp"



using ::testing::Exactly;
using ::testing::Return;

TEST(LinSM_DecrementTest, ErrorReport) {
    uint16_t timer = 1;
    uint16_t* p_timer = &timer;

    EXPECT_CALL(MyDet, Det_ReportRuntimeError(255, 0 , 0x30, 0))
        .Times(Exactly(1));
    DecrementTimer(p_timer);
}

TEST(LinSM_DecrementTest, NoErrorReport) {
    uint16_t timer = 4;
    uint16_t* p_timer = &timer;

    EXPECT_CALL(MyDet, Det_ReportRuntimeError(255, 0, 0x30, 0))
        .Times(Exactly(0));
    DecrementTimer(p_timer);
}


TEST(LinSM_Init_DeInit_funct, InitDeInit)
{
    EXPECT_EQ(LINSM_UNINIT, LinSM_state);
    LinSM_Init(NULL);
    EXPECT_EQ(LINSM_INIT, LinSM_state);
    LinSM_DeInit();
    EXPECT_EQ(LINSM_UNINIT, LinSM_state);
}

TEST(LinSM_ScheduleRequest_funct, WrongState)
{
    NetworkHandleType network = 0;
    LinIf_SchHandleType schedule = 1;
    LinSM_Init(NULL);
    EXPECT_EQ(E_NOT_OK, LinSM_ScheduleRequest(network, schedule));
}

TEST(LinSM_ScheduleRequest_funct, GoodStateNoAck)
{
    NetworkHandleType network = 0;
    LinIf_SchHandleType schedule = 1;
    Std_ReturnType result;

    EXPECT_CALL(MyLinIf, LinIf_ScheduleRequest(network, schedule))
        .Times(Exactly(1))
        .WillOnce(Return(E_NOT_OK));
    LinSM_Init(NULL);
    NetworkStatusTable[network].LinSM_substate = LINSM_FULL_COM;
    result = LinSM_ScheduleRequest(network, schedule);
    EXPECT_EQ(result, E_NOT_OK);
}

TEST(LinSM_ScheduleRequest_funct, GoodStateAck)
{
    NetworkHandleType network = 0;
    LinIf_SchHandleType schedule = 1;
    Std_ReturnType result;

    EXPECT_CALL(MyLinIf, LinIf_ScheduleRequest(network, schedule))
        .Times(Exactly(1))
        .WillOnce(Return(E_OK));
    LinSM_Init(NULL);
    NetworkStatusTable[network].LinSM_substate = LINSM_FULL_COM;
    result = LinSM_ScheduleRequest(network, schedule);
    EXPECT_EQ(result, E_OK);
}

TEST(LinSM_RequestComMode_funct, fullComOK)
{
    NetworkHandleType network = 0;
    ComM_ModeType mode = COMM_FULL_COMMUNICATION;
    Std_ReturnType result;

    EXPECT_CALL(MyLinIf, LinIf_WakeUp(network))
        .Times(Exactly(1))
        .WillOnce(Return(E_OK));

    LinSM_Init(NULL);
    result = LinSM_RequestComMode(network, mode);
    EXPECT_EQ(result, E_OK);

}

TEST(LinSM_RequestComMode_funct, fullComNOTOK)
{
    NetworkHandleType network = 0;
    ComM_ModeType mode = COMM_FULL_COMMUNICATION;
    Std_ReturnType result;

    EXPECT_CALL(MyLinIf, LinIf_WakeUp(network))
        .Times(Exactly(1))
        .WillOnce(Return(E_NOT_OK));

    EXPECT_CALL(MyBswM_LinSM, BswM_LinSM_CurrentState(network, LINSM_NO_COM))
        .Times(Exactly(1));
    EXPECT_CALL(MyComM, ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION))
        .Times(Exactly(1));

    LinSM_Init(NULL);
    result = LinSM_RequestComMode(network, mode);
    EXPECT_EQ(result, E_NOT_OK);

}

TEST(LinSM_GetCurrentComMode_funct, fullCom)
{
    NetworkHandleType network = 0;
    ComM_ModeType mode;
    ComM_ModeType* p_mode = &mode;
    Std_ReturnType result;

    EXPECT_CALL(MyLinIf, LinIf_WakeUp(network))
        .Times(Exactly(1))
        .WillOnce(Return(E_OK));
    LinSM_Init(NULL);
    LinSM_RequestComMode(network, COMM_FULL_COMMUNICATION);
    LinSM_WakeupConfirmation(network, true);
    result = LinSM_GetCurrentComMode(network, p_mode);
    EXPECT_EQ(result, E_OK);
    EXPECT_EQ(*p_mode, COMM_FULL_COMMUNICATION);
    EXPECT_EQ(NetworkStatusTable[network].LinSM_substate, LINSM_FULL_COM);
    
}

TEST(LinSM_GetCurrentComMode_funct, fullComError)
{
    NetworkHandleType network = 0;
    ComM_ModeType mode;
    ComM_ModeType* p_mode = &mode;
    Std_ReturnType result;

    EXPECT_CALL(MyLinIf, LinIf_WakeUp(network))
        .Times(Exactly(1))
        .WillOnce(Return(E_OK));
    LinSM_Init(NULL);
    LinSM_RequestComMode(network, COMM_FULL_COMMUNICATION);
    LinSM_WakeupConfirmation(network, false);
    result = LinSM_GetCurrentComMode(network, p_mode);
    EXPECT_EQ(result, E_OK);
    EXPECT_EQ(*p_mode, COMM_NO_COMMUNICATION);
    EXPECT_EQ(NetworkStatusTable[network].LinSM_substate, LINSM_NO_COM);

}

TEST(LinSM_MainFunction_funct, TimersOff)
{
    NetworkHandleType network = 0;
    LinIf_SchHandleType schedule = 0;
    EXPECT_CALL(MyBswM_LinSM, BswM_LinSM_CurrentState(network, LINSM_FULL_COM))
        .Times(Exactly(0));
    EXPECT_CALL(MyComM, ComM_BusSM_ModeIndication(network, COMM_FULL_COMMUNICATION))
        .Times(Exactly(0));

    EXPECT_CALL(MyBswM_LinSM, BswM_LinSM_CurrentState(network, LINSM_NO_COM))
        .Times(Exactly(0));
    EXPECT_CALL(MyComM, ComM_BusSM_ModeIndication(network, COMM_NO_COMMUNICATION))
        .Times(Exactly(0));

    EXPECT_CALL(MyBswM_LinSM, BswM_LinSM_CurrentState(network, LINSM_NO_COM))
        .Times(Exactly(0));
    EXPECT_CALL(MyBswM_LinSM, BswM_LinSM_CurrentSchedule(network, schedule))
        .Times(Exactly(0));

    LinSM_Init(NULL);
    for (int i = 0; i < 10; i++)
    {
        LinSM_MainFunction();
    }
}