#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "src/gmock-cardinalities.cc"
#include "src/gmock-internal-utils.cc"
#include "src/gmock-matchers.cc"
#include "src/gmock-spec-builders.cc"
#include "src/gmock.cc"

#include "src/gtest.cc"
#include "src/gtest-death-test.cc"
#include "src/gtest-filepath.cc"
#include "src/gtest-port.cc"
#include "src/gtest-printers.cc"
#include "src/gtest-test-part.cc"
#include "src/gtest-typed-test.cc"


class Turtle {
public:
    virtual ~Turtle() {}
    virtual int PenUp() = 0;
};

class MockTurtle : public Turtle {
public:

    //MOCK_METHOD0(PenUp, void()); // working =)
    MOCK_METHOD(int, PenUp, (), (override)); // working =)

};

int main(int, const char* []) {
    return 0;
}